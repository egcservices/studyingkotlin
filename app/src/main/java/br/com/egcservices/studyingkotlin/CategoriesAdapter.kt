package br.com.egcservices.studyingkotlin

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_category.view.*

class CategoriesAdapter(private val categories: List<Category>, private val context: Context) :
        Adapter<CategoriesAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val category = categories[position]
        holder.name.text = category.name
//        holder?.let {
//            it.name.text = category.name
//        }
//        holder?.name?.text = category.name
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_category, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.categoryName
        /* To use this function, comment holder?.let in onBindViewHolder
        fun bindView(category: Category) {
            val name = itemView.categoryName
            name.text = category.name
        }*/
    }
}

