package br.com.egcservices.studyingkotlin

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView = list_categories
        val categories = categories()
        if (categories.isNotEmpty()) {
            val progress = rlProgress
            progress.visibility = View.GONE
        }
        recyclerView.adapter = CategoriesAdapter(categories, this)
        recyclerView.layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
    }

    private fun categories(): List<Category> {
        return listOf(
                Category(1, "Teste1"),
                Category(2, "Teste2"),
                Category(3, "Teste3"))
    }
}